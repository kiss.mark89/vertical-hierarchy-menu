import React, { useState, useCallback, useEffect } from "react";
import ArrowSvg from "../../images/arrow.svg";

const MenuItem = ({
  colours,
  item: { label, id, sublevels = [] },
  onSelect,
  level = -1
}) => {
  const [classLevel, setClassLevel] = useState(level);
  const [expanded, setExpanded] = useState();
  const hasSubmenu = sublevels.length > 0;
  const renderSubmenu = expanded && hasSubmenu;

  const toggle = useCallback(
    () => {
      setExpanded(!expanded);
    },
    [expanded]
  );

  useEffect(() => {
    setClassLevel(classLevel => classLevel + 1);
  }, []);

  return (
    <li
      className={`menu-item ${hasSubmenu && `has-submenu`} ${expanded && `expanded`} level-${classLevel}`}
      style={{ color: colours?.length ? colours[classLevel] : "#232323" }}
    >
      <span
        className="menu-item-title"
        onClick={hasSubmenu ? toggle : () => onSelect(id)}
      >
        <span className="label" title={label}>
          {label}
        </span>
        {hasSubmenu && <Arrow expanded={expanded} />}
      </span>
      {renderSubmenu && (
        <Hierarchy
          colours={colours}
          sublevels={sublevels}
          onSelect={onSelect}
          level={classLevel}
        />
      )}
    </li>
  );
};

const Hierarchy = ({ colours, sublevels, onSelect, level }) =>
  !sublevels?.length ? null : (
    <ul className="tree">
      {sublevels.map(item => (
        <MenuItem
          colours={colours}
          key={item.id}
          item={item}
          onSelect={onSelect}
          level={level}
        />
      ))}
    </ul>
  );

const Arrow = expanded => (
  <img
    src={ArrowSvg}
    alt="arrow"
    className={expanded.expanded ? `arrow expanded` : "arrow"}
  />
);

export default Hierarchy;
