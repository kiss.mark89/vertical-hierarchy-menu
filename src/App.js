import React from "react";
import "./App.scss";
import Hierarchy from "./components/Hierarchy/Hierarchy";

const mockData = [
  {
    label: "Streetscape",
    id: "streetscape",
    sublevels: [
      {
        label: "SS Central South",
        id: "ss_central_south",
        sublevels: [{ label: "SS CN Ang Mo Kio", id: "ss_cn_ang_mo_kio" }]
      },
      {
        label: "Next item",
        id: "next_item",
        sublevels: [
          {
            label: "Parent 1",
            id: "parent1",
            sublevels: [{ label: "Child 1", id: "child_1" }]
          }
        ]
      },
      {
        label: "Last item",
        id: "last_item",
        sublevels: [
          {
            label: "Parent 2",
            id: "parent2",
            sublevels: [{ label: "Child 2", id: "child_2" }]
          },
          {
            label: "Parent 3",
            id: "parent_3",
            sublevels: [
              {
                label: "Child 3",
                id: "child_3",
                sublevels: [{ label: "End", id: "end" }]
              }
            ]
          }
        ]
      }
    ]
  }
];

function App() {
  return (
    <Hierarchy
      colours={["green", "orange", "purple", "blue", "#bada55"]}
      sublevels={mockData}
      onSelect={level => alert(level)}
    />
  );
}

export default App;
